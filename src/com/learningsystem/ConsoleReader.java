package com.learningsystem;

import java.util.Scanner;

public class ConsoleReader {
    private ConsoleReader() {
    }

    private static Scanner scanner = new Scanner(System.in);

    public static String readString() {
        return scanner.nextLine();
    }

    public static <T> int readInt(T[] array) {
        int enteredNumber = 0;
        if (array.length != 0) {
            while (!scanner.hasNextInt()) {
                System.out.println("Enter numeric value!");
                scanner.next();
            }
            enteredNumber = scanner.nextInt();
            if (enteredNumber >= 0 && enteredNumber <= array.length - 1) {
                return enteredNumber;
            } else {
                System.out.println("Enter correct value!");
                return readInt(array);
            }
        }
        return enteredNumber;
    }
}
