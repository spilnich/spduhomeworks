package com.learningsystem;


public class ConsoleMenu {
    private final LectureOperator lectureOperator = new LectureOperator();
    private final LiteratureOperator literatureOperator = new LiteratureOperator();

    public void showMainLectureMenu() {

        System.out.println();
        System.out.println("1. Show all lectures");
        System.out.println("2. Add lecture");
        System.out.println("3. Delete lecture");
        System.out.println("4. Choose lecture");
        System.out.println("5. Exit");
        System.out.println();

    }

    public void showAdditionalLiteratureMenu() {

        System.out.println();
        System.out.println("1. Show list of literature");
        System.out.println("2. Add new literature");
        System.out.println("3. Delete literature");
        System.out.println("4. Back");
        System.out.println();

    }

    public void chooseLectureMenuOption() {
        String inputData = "";
        while (!(inputData = ConsoleReader.readString()).equals("5")) {

            lectureOperator.removeNullValues();
            lectureOperator.recycleIndexesOfLectures();

            switch (inputData) {
                case "1":
                    lectureOperator.showAllLectures();
                    showMainLectureMenu();
                    break;
                case "2":
                    System.out.println("Enter lecture's title:");
                    String lectureTitle = ConsoleReader.readString();
                    Lecture lecture = new Lecture();
                    lecture.setTitle(lectureTitle);
                    lectureOperator.addLecture(lecture);
                    showMainLectureMenu();
                    break;
                case "3":
                    System.out.println("Enter lecture's number:");
                    int index = ConsoleReader.readInt(lectureOperator.getLectures());
                    lectureOperator.deleteLecture(index);
                    showMainLectureMenu();
                    break;
                case "4":
                    System.out.println("Enter lecture's index:");
                    int indexOfChosenLecture = ConsoleReader.readInt(lectureOperator.getLectures());
                    showAdditionalLiteratureMenu();
                    chooseLiteratureMenuOption(indexOfChosenLecture);
                    break;
                default:
                    chooseLectureMenuOption();
                    break;
            }
        }
    }

    public void chooseLiteratureMenuOption(int index) {
        String inputData = "";
        String literatureName = "";
        while (!(inputData = ConsoleReader.readString()).equals("5")) {

            literatureOperator.removeNullLiteratureValues(index);

            switch (inputData) {
                case "1":
                    literatureOperator.showListOfLiterature(index);
                    showAdditionalLiteratureMenu();
                    break;
                case "2":
                    System.out.println("Enter literature's name:");
                    literatureName = ConsoleReader.readString();
                    Literature literature = new Literature(literatureName);
                    literature.setName(literatureName);
                    literatureOperator.addNewLiterature(literature, index);
                    showAdditionalLiteratureMenu();
                    break;
                case "3":
                    System.out.println("Enter literature's index:");
                    int literatureIndex = ConsoleReader.readInt(literatureOperator.getListOfLiterature(index));
                    literatureOperator.deleteLiteratureByIndex(index, literatureIndex);
                    showAdditionalLiteratureMenu();
                    break;
                case "4":
                    showMainLectureMenu();
                    chooseLectureMenuOption();
                    break;
                default:
                    break;
            }
        }
    }
}
