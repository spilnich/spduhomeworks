package com.learningsystem;

import java.util.Arrays;
import java.util.Objects;

public class LiteratureOperator {

    private Literature[][] literature;

    LiteratureOperator() {
        int lectureArrayLength = new LectureOperator().getLectures().length;
        literature = new Literature[lectureArrayLength][10];
    }

    public void showListOfLiterature(int index) {
        System.out.println(Arrays.deepToString(literature[index]));
    }

    public Literature[] getListOfLiterature(int index) {
        return literature[index];
    }

    public void addNewLiterature(Literature resource, int index) {
        if (literature[index].length == 0) {
            literature[index] = new Literature[10];
        }
        for (int literatureIndex = 0; literatureIndex < literature[index].length; literatureIndex++) {
            int lastIndex = 0;
            Literature temp = literature[index][literature[index].length - 1];
            if (literature[index][literatureIndex] == temp) {
                lastIndex = literatureIndex;
            }
            if (lastIndex == literature[index].length - 1) {
                literature[index] = resizeLiteratureArray(index);
            }
            if (literature[index][literatureIndex] == null) {
                literature[index][literatureIndex] = resource;
                System.out.println("Literature was added");
                break;
            }
        }
    }

    public void deleteLiteratureByIndex(int lectureIndex, int literatureIndex) {
        if (literature[lectureIndex].length == 0) {
            System.out.println("There is nothing to delete.");
            return;
        }
        literature[lectureIndex][literatureIndex] = null;
        System.out.println("Literature was removed");
    }

    private Literature[] resizeLiteratureArray(int index) {
        int newLength = literature[index].length * 2;

        return Arrays.copyOf(literature[index], newLength);
    }

    public void removeNullLiteratureValues(int index) {
        literature[index] = Arrays.stream(literature[index]).filter(Objects::nonNull).toArray(Literature[]::new);
    }
}
