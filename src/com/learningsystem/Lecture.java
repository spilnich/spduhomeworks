package com.learningsystem;


public class Lecture {
    private String title;
    private int index;

    public Lecture() {
    }

    public Lecture(String title, int index) {
        this.title = title;
        this.index = index;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    @Override
    public String toString() {
        return "Lecture " + index + " {" + title + "}";
    }
}
