package com.learningsystem;

public class Literature {
    private String name;

    public Literature(String name) {
        this.name = name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
