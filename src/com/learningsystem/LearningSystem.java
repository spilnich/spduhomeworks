package com.learningsystem;

public class LearningSystem {
    public static void main(String[] args) {
        ConsoleMenu consoleMenu = new ConsoleMenu();

        consoleMenu.showMainLectureMenu();
        consoleMenu.chooseLectureMenuOption();
    }
}
