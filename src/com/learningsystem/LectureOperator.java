package com.learningsystem;

import java.util.Arrays;
import java.util.Objects;

public class LectureOperator {

    private int lectureCount;
    private Lecture[] lectures;

    public LectureOperator() {
        lectures = new Lecture[10];
        lectures[0] = new Lecture("Java (Core): L1 - Intro. Java Basics", lectureCount);
        lectures[1] = new Lecture("Java (Common): L1 - Intellij IDEA Features. GitLab flow", ++lectureCount);
        lectures[2] = new Lecture("Tech Skills: L2 - Web basics", ++lectureCount);
        lectures[3] = new Lecture("Java (Core): L2 - Core Java API", ++lectureCount);
    }

    public Lecture[] getLectures() {
        return lectures;
    }

    public void showAllLectures() {
        for (Lecture lecture : lectures) {
            System.out.println(lecture);
        }
    }

    public void addLecture(Lecture lecture) {
        if (lectures.length == 0) {
            lectureCount = -1;
            lectures = new Lecture[10];
        }
        lecture.setIndex(++lectureCount);

        if (lectureCount == lectures.length) {
            lectures = resizeLectureArray();
        }

        lectures[lectureCount] = lecture;
        System.out.println("Lecture was added");
    }

    private Lecture[] resizeLectureArray() {
        int newLength = lectures.length * 2;

        return Arrays.copyOf(lectures, newLength);
    }

    public void deleteLecture(int index) {
        if (lectures.length == 0) {
            System.out.println("There is nothing to delete.");
            return;
        }
        lectures[index] = null;
        --lectureCount;
        System.out.println("Lecture was removed");
    }

    public void removeNullValues() {
        lectures = Arrays.stream(lectures).filter(Objects::nonNull).toArray(Lecture[]::new);
    }

    public void recycleIndexesOfLectures() {
        for (int lectureIndex = 0; lectureIndex < lectures.length; lectureIndex++) {
            lectures[lectureIndex].setIndex(lectureIndex);
        }
    }
}
